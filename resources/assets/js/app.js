
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import CarsIndex from './components/cars/CarsIndex.vue';
import CarsCreate from './components/cars/CarsCreate.vue';
import CarsEdit from './components/cars/CarsEdit.vue';

const routes = [
    {
        path: '/',
        components: {
            carsIndex: CarsIndex
        }
    },
    {path: '/admin/cars/create', component: CarsCreate, name: 'createCar'},
    {path: '/admin/cars/edit/:id', component: CarsEdit, name: 'editCar'},
]
 
const router = new VueRouter({ routes })
 
const app = new Vue({ router }).$mount('#app')