<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarsFleet extends Model
{
    protected $fillable =['category', 'fuel', 'seats', 'transmission'];
}
